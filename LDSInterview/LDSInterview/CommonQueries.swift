//
//  CommonQueries.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import Foundation
import CoreData

class CommonQueries {
    
    class func fetchIndividual(forId uniqueId: Int64, inContext context: NSManagedObjectContext) -> Individual {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Individual")
        let predicate = NSPredicate(format: "uniqueId = %i", uniqueId)
        request.predicate = predicate
        
        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                return results.first as! Individual
            }else {
                return Individual(context: context)
            }
        }catch let err as NSError {
            print(err)
            return Individual(context: context)
        }
        
    }
    
}
