//
//  WelcomeView.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class WelcomeView: UIViewController {
    
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var lightButton: UIButton!
    @IBOutlet weak var darkButton: UIButton!
    
    @IBOutlet weak var v1: UIView!
    @IBOutlet weak var v2: UIView!
    @IBOutlet weak var v3: UIView!
    @IBOutlet weak var v4: UIView!
    
    override func viewDidLoad() {
        
        self.title = ""
        
        let videoURL = Bundle.main.url(forResource: "episode1clip", withExtension: "mov")
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        self.view.bringSubview(toFront: self.coverView)
        player.play()
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { _ in
            player.seek(to: kCMTimeZero)
            player.play()
        }
        
        styleView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func styleView() -> Void {
        
        v1.layer.cornerRadius = 70.0
        v2.layer.cornerRadius = 55.0
        v3.layer.cornerRadius = 40.0
        v4.layer.cornerRadius = 25.0
        
        lightButton.layer.cornerRadius = 30.0
        darkButton.layer.cornerRadius = 30.0
        
        darkButton.backgroundColor = UIColor.clear
        darkButton.layer.borderColor = UIColor.white.cgColor
        darkButton.layer.borderWidth = 2
        
    }
    
    @IBAction func lightSideTapped() -> Void {
        let vc = IndividualList()
        vc.isLight = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func darkSideTapped() -> Void {
        let vc = IndividualList()
        vc.isLight = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
