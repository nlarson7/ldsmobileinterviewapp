//
//  IndividualDetail.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit

class IndividualDetail: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var individualName: UILabel!
    @IBOutlet weak var affiliation: UILabel!
    @IBOutlet weak var birthDate: UILabel!
    @IBOutlet weak var forceSensitive: UILabel!
    
    var individual: Individual!
    
    override func viewDidLoad() {
        
        
        self.styleView()
        
        if individual.profilePicture != nil {
            self.profileImage.image = UIImage(data: individual.profilePicture!)
        }
        
        individualName.text = (individual.firstName ?? "").appending(" ").appending(individual.lastName ?? "")
        affiliation.text = individual.affiliation
        birthDate.text = individual.birthdate
        forceSensitive.text = individual.forceSensitive ? "Yes" : "No"
        
    }
    
    func styleView() -> Void {
        
        self.profileImage.layer.cornerRadius = 45.0
        self.profileImage.layer.masksToBounds = true
        self.profileImage.layer.borderColor = UIColor.white.cgColor
        self.profileImage.layer.borderWidth = 2.0
        
    }
    
}
