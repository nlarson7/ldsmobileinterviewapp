//
//  LDSIndividualHandle.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import Foundation
import CoreData

extension LDSHandle {
    
    class func getIndividuals() -> Void {
        
        let endpoint = self.baseUrl.appending(String(format: "%@", API.individuals.rawValue))
        let params = APIRequestParameters(endpoint: endpoint, httpMethod: "GET", API: .individuals, additionalParameters: nil)
        
        let op = APIRequest(parameters: params)
        QueueManager.shared.downloadQueue.addOperation(op)
        
    }
    
    func handleIndividuals(downloadedJson json: [String: Any], parameters: APIRequestParameters, completed: OperationCompleted?) -> Void {
        
        if let allIndividuals = json["individuals"] as? [[String: Any]] {
            
            let context = CoreDataStack.shared.privateContext()
            
            for anIndividual in allIndividuals {
                
                if let uniqueId = anIndividual["id"] as? Int64 {
                    
                    let individual = CommonQueries.fetchIndividual(forId: uniqueId, inContext: context)
                    individual.uniqueId = uniqueId
                    individual.firstName = anIndividual["firstName"] as? String
                    individual.lastName = anIndividual["lastName"] as? String
                    individual.affiliation = anIndividual["affiliation"] as? String
                    individual.forceSensitive = (anIndividual["forceSensitive"] as? Int)! == 1 ? true : false
                    individual.profilePictureURL = anIndividual["profilePicture"] as? String
                    individual.birthdate = anIndividual["birthdate"] as? String
                    
                }
                
            }
            
            if context.hasChanges {
                context.perform {
                    do {
                        try context.save()
                    }catch let err as NSError {
                        print(err)
                    }
                    CoreDataStack.shared.saveContext(completed: {
                        completed?()
                    })
                }
            }
            
        }else {
            completed?()
        }
        
    }
    
}
