//
//  APIRequest.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit

class APIRequest: Operation {
    
    let parameters: APIRequestParameters
    
    var operationExecuting: Bool = true
    var operationFinished: Bool = false
    
    init(parameters: APIRequestParameters) {
        self.parameters = parameters
    }
    
    override var isExecuting: Bool {
        return operationExecuting
    }
    
    override var isFinished: Bool {
        return operationFinished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    func completeOperation() -> Void {
        willChangeValue(forKey: "isFinished")
        willChangeValue(forKey: "isExecuting")
        operationExecuting = false
        operationFinished = true
        didChangeValue(forKey: "isFinished")
        didChangeValue(forKey: "isExecuting")
    }
    
    override func main() {
        
        if self.isCancelled {
            willChangeValue(forKey: "isFinished")
            operationFinished = true
            didChangeValue(forKey: "isFinished")
            return
        }
        
        willChangeValue(forKey: "isExecuting")
        operationExecuting = true
        didChangeValue(forKey: "isExecuting")
        
        guard let url = URL(string: parameters.endpoint) else {
            print("Error: cannot create URL")
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = parameters.httpMethod
        
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            let windowRootVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
            
            guard error == nil else {
                
                let alert = UIAlertController(title: "Error", message: "There was a problem accessing the API. Please check your internet connection and try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
                windowRootVC?.present(alert, animated: true, completion: nil)
                
                self.completeOperation()
                print(error!)
                
                return
            }
            
            guard let responseData = data else {
                
                let alert = UIAlertController(title: "Error", message: "There was a problem accessing the API. Please check your internet connection and try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
                windowRootVC?.present(alert, animated: true, completion: nil)
                
                self.completeOperation()
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.init(rawValue: 0))
                if let dict = json as? Dictionary<String, Any> {
                    self.handleJson(json: dict)
                }else {
                    self.completeOperation()
                }
            }catch let err as NSError {
                
                let alert = UIAlertController(title: "Error", message: "There was a problem accessing the API. Please check your internet connection and try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
                windowRootVC?.present(alert, animated: true, completion: nil)
                
                print(err)
                self.completeOperation()
                
            }
            
        }
        
        task.resume()
        
    }
    
    func handleJson(json: [String: Any]) -> Void {
        if self.parameters.API == .individuals {
            LDSHandle().handleIndividuals(downloadedJson: json, parameters: self.parameters, completed: {
                self.completeOperation()
            })
        }
    }
    
}


