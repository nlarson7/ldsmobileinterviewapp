//
//  APIHelpers.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import Foundation

typealias OperationCompleted = () -> ()
var operationCompleted: OperationCompleted? = { }

struct APIRequestParameters {
    var endpoint: String!
    var httpMethod: String!
    var API: API
    var additionalParameters: [String:Any]?
}

enum API: String {
    case individuals = "mobile/interview/directory"
}
