//
//  IndividualCell.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit
import CoreData

class IndividualCell: UITableViewCell {
    
    @IBOutlet weak var individualName: UILabel!
    @IBOutlet weak var affiliation: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    func configureCell(withIndividual individual: Individual) -> Void {
        
        individualName.text = (individual.firstName ?? "").appending(" ").appending(individual.lastName ?? "")
        affiliation.text = individual.affiliation
        
        profileImageView.layer.cornerRadius = 20.0
        profileImageView.layer.masksToBounds = true
        
        if individual.profilePicture == nil {
            self.profileImageView.image = nil
            self.downloadImage(individual: individual)
        }else {
            let image = UIImage(data: individual.profilePicture!)
            self.profileImageView.image = image
        }
        
        
        
    }
    
    func downloadImage(individual: Individual) -> Void {
        
        let profilePicURL = individual.profilePictureURL!
        let individualId = individual.uniqueId
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let imgData = NSData(contentsOf: URL(string: profilePicURL)!)
            
            let context = CoreDataStack.shared.privateContext()
            
            let anIndividual = CommonQueries.fetchIndividual(forId: individualId, inContext: context)
            anIndividual.profilePicture = imgData as Data?
            
            context.perform {
                do {
                    try context.save()
                }catch let err as NSError {
                    print(err)
                }
                CoreDataStack.shared.saveContext(completed: {
                    let anImage = UIImage(data: imgData! as Data)
                    DispatchQueue.main.async {
                        self.profileImageView.image = anImage
                    }
                })
            }
            
        }
        
    }
    
}
