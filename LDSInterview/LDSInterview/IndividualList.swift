//
//  IndividualList.swift
//  LDSInterview
//
//  Created by Nathan Larson on 11/30/17.
//  Copyright © 2017 Nathan Larson. All rights reserved.
//

import UIKit
import CoreData

class IndividualList: UITableViewController {
    
    var fetchedResultsController: NSFetchedResultsController<Individual> = NSFetchedResultsController()
    var isLight: Bool!
    
    override func viewDidLoad() {
        
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        UIColor.init(named: "purpleBar")?.setFill()
        UIRectFill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.title = "Individuals"
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: String(describing: IndividualCell.self), bundle: nil), forCellReuseIdentifier: String(describing: IndividualCell.self))
        tableView.separatorInset = UIEdgeInsets.zero
        
        if self.isLight {
            self.tableView.backgroundColor = UIColor.white
        }else {
            self.tableView.backgroundColor = UIColor.black
        }
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func configureView() {
        fetchedResultsController = listFetchedResultsController()
    }
    
}

//  MARK: TableView methods

extension IndividualList {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 91.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: IndividualCell.self), for: indexPath) as! IndividualCell
        
        cell.configureCell(withIndividual: fetchedResultsController.object(at: indexPath))
        
        if !isLight {
            cell.backgroundColor = UIColor.black
            cell.individualName.textColor = UIColor.red
            cell.affiliation.textColor = UIColor.red
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IndividualDetailView") as! IndividualDetail
        vc.individual = fetchedResultsController.object(at: indexPath)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

private extension IndividualList {
    
    func listFetchedResultsController() -> NSFetchedResultsController<Individual> {
        let fetchedResultController = NSFetchedResultsController(fetchRequest: listFetchRequest(),
                                                                 managedObjectContext: CoreDataStack.shared.mainContext,
                                                                 sectionNameKeyPath: nil,
                                                                 cacheName: nil)
        fetchedResultController.delegate = self
        
        do {
            try fetchedResultController.performFetch()
        } catch let error as NSError {
            fatalError("Error: \(error.localizedDescription)")
        }
        return fetchedResultController
    }
    
    func listFetchRequest() -> NSFetchRequest<Individual> {
        let fetchRequest = NSFetchRequest<Individual>(entityName: "Individual")
        fetchRequest.fetchBatchSize = 25
        
        let sortDescriptor = NSSortDescriptor(key: "firstName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        return fetchRequest
    }
}

extension IndividualList: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.reloadData()
    }
}
